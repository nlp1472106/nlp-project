import { createRouter, createWebHistory } from 'vue-router'
import NavBar from '../views/HomeView.vue'
import HistoryTag from '@/components/HistoryTag.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'navbar',
      component: NavBar
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
   

    },


    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },

    {

      path: '/historytag',
      name: 'historytag',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../components/SeeAll.vue'),
        header: () => import('../views/HomeView.vue')
      },
      meta: {
        layout: 'NavBar'
      }
    },
    {
      path: '/seeAll',
      name: 'seeAll',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../components/SeeAll.vue'),
        header: () => import('../views/HomeView.vue')
      },
      meta: {
        layout: 'NavBar'
      }
    }, 
    {
      path: '/',
      name: 'postmain',
      components: {
        default: () => import('../components/PostMain.vue'),
        // header: () => import('../views/HomeView.vue')
      },
      meta: {
        layout: 'NavBar'
      }

    },

  ]
})

export default router
